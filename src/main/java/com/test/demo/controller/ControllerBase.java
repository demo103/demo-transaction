package com.test.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.demo.entity.Store;
import com.test.demo.repository.TransactionRepository;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@Service
@RequestMapping("/transactionservice")
public class ControllerBase {

    @Autowired
    private TransactionRepository transactionRepo;

    @GetMapping("/countChar")
    public String countChar(@RequestParam String kata) {
        JSONObject result = new JSONObject();

        if(kata.isEmpty()) {
            result.put("status",HttpStatus.BAD_REQUEST );
            return result.toString();
        }

        int count = 0;
        char prev = kata.charAt(0);
        String newStr = "";
        for (int i = 0; i < kata.length(); i++) {
            char current = kata.charAt(i);
            if(prev == kata.charAt(i)) {
                count++;
            } else {
                newStr += prev;
                newStr += count;

                count = 0;
                prev = current;
                if(prev == kata.charAt(i)) {
                    count++;
                }
            }
        }

        newStr += prev;
        newStr += count == 1 ? "" : count;

        HashMap map = new HashMap();

        result.put("status", HttpStatus.OK);
        result.put("data", newStr);
        return result.toString();
    }


    @PutMapping("/transaction/{transaction_id}")
    public String storeTransaction(@PathVariable Long transaction_id, @RequestBody JSONObject body) {
        JSONObject result = new JSONObject();
        result.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            Store store = new Store();
            store.setTransactionId(transaction_id);
            store.setParentId(Long.parseLong(body.get("parent_id").toString()));
            store.setAmount(Double.parseDouble(body.get("amount").toString()));
            store.setType(body.get("type").toString());
            transactionRepo.save(store);

            result.put("status", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    @GetMapping("/transaction/{transaction_id}")
    public String getTransaction(@PathVariable Long transaction_id) {
        JSONObject result = new JSONObject();
        result.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Store> storeList = transactionRepo.findByTransactionId(transaction_id);
            result.put("data", mapper.writeValueAsString(storeList));
            result.put("status", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @GetMapping("/types/{type}")
    public String getTransactionType(@PathVariable String type) {
        JSONObject result = new JSONObject();
        result.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Store> storeList = transactionRepo.findByType(type);
            result.put("data", mapper.writeValueAsString(storeList));
            result.put("status", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @GetMapping("/sum/{transaction_id}")
    public String sumTransaction(@PathVariable Long transaction_id) {
        JSONObject result = new JSONObject();
        result.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            result.put("data", transactionRepo.sumTransactionById(transaction_id));
            result.put("status", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
