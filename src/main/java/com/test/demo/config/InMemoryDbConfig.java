package com.test.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:application-local.yml"})
@EnableJpaRepositories(
    basePackages = "repository",
    entityManagerFactoryRef = "inMemoryEntityManager",
    transactionManagerRef = "inMemoryTransactionManager"
)
public class InMemoryDbConfig {

  private final Environment environment;
  public InMemoryDbConfig(Environment environment) {
    this.environment = environment;
  }

  /**
   * Handle Database configuration
   *
   * @return Datasource object.
   */
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource")
  public DataSource productDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setSchema(environment.getProperty("spring.datasource.name"));
    dataSource.setDriverClassName("org.h2.Driver");
    dataSource.setUrl("jdbc:h2:file:../h2/data;DB_CLOSE_ON_EXIT=FALSE;AUTO_SERVER=TRUE;INIT=CREATE SCHEMA IF NOT EXISTS data_details");
    dataSource.setUsername("sa");
    dataSource.setPassword("");

    return dataSource;
  }

  /**
   * Handle Entity Manager object base on bean configuration
   *
   * @return Entity manager object
   */
  @Bean
  public LocalContainerEntityManagerFactoryBean inMemoryEntityManager() {
    LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
    entityManager.setDataSource(productDataSource());
    entityManager.setPackagesToScan(new String[]{"entity.inmemory"});

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    entityManager.setJpaVendorAdapter(vendorAdapter);

    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", "update");
    properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
    entityManager.setJpaPropertyMap(properties);

    return entityManager;
  }

  /**
   * Handle transaction manager activity
   *
   * @return Transaction manager object
   */
  @Bean
  public PlatformTransactionManager inMemoryTransactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(inMemoryEntityManager().getObject());
    return transactionManager;
  }

}