package com.test.demo.repository;

import com.test.demo.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface TransactionRepository extends JpaRepository<Store, Long> {

    List<Store> findByTransactionId(Long transactionId);
    List<Store> findByType(String type);

    @Query(value = "SELECT SUM(amount) FROM STORE where transaction_id = ?1", nativeQuery = true)
    String sumTransactionById(Long transactionId);

}
